
from django.db import models
from django.utils import timezone


class AbstractBaseModel(models.Model):

    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    def save(self, *args, **kwargs):

        if not self.pk:
            self.created = timezone.now()
        self.modified = timezone.now()

        return super().save(*args, **kwargs)

    class Meta:
        abstract = True
