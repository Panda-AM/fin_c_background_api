
from rest_framework import serializers

from apps.users.models import User


class UserSerializer(serializers.ModelSerializer):

    def validate(self, data):

        data['username'] = data['email']
        return data

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', )
