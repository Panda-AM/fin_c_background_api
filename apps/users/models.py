
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models

from apps.core.models import AbstractBaseModel


class User(AbstractUser, AbstractBaseModel):

    email = models.EmailField(unique=True, error_messages={'unique': _('A user with that email already exists.')})

    def __str__(self):
        return self.email
