
from django.test import TestCase

from faker import Faker

from apps.users.tasks import users_bulk_creator, user_creator


class UsersBulkCreatorTestCase(TestCase):

    def __init__(self, *args, **kwargs):

        self.users = None
        super().__init__(*args, **kwargs)

    def setUp(self):
        faker = Faker()
        self.users = [{
            'first_name': faker.first_name(),
            'last_name': faker.last_name(),
            'email': faker.email(),
        }]

    def test_users_bulk_creator(self):
        users_bulk_creator(self.users)


class UsersCreatorTestCase(TestCase):

    def __init__(self, *args, **kwargs):

        self.user = None
        super().__init__(*args, **kwargs)

    def setUp(self):
        faker = Faker()
        self.user = {
            'first_name': faker.first_name(),
            'last_name': faker.last_name(),
            'email': faker.email(),
        }

    def test_valid_user_creator(self):
        assert user_creator(self.user) is True

    def test_in_valid_user_creator(self):

        self.user['email'] = 'invalid_email'
        assert user_creator(self.user) is False
