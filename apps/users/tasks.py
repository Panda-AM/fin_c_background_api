from celery import task

from apps.users.serializers import UserSerializer


@task.task(name='users_bulk_creator')
def users_bulk_creator(users):
    for user in users:
        user_creator.delay(user)


@task.task(name='user_creator')
def user_creator(user):

    serializer = UserSerializer(data=user)

    # TODO send notification about the user creation results.
    if serializer.is_valid():
        serializer.save()
        # send_notification()
        return True
    else:
        # send_notification(serializer.errors)
        return False
