
import os

from django.conf import settings
from celery import Celery


settings_file = 'local' if os.environ.get('IS_LOCAL') else 'production'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fin_c_background_api.settings." + settings_file)

celery_app = Celery(broker=settings.CELERY_BROKER_URL)
celery_app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
